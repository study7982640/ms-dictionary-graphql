# Query.entries: [Entry]
                 
## Arguments
| Name | Description | Required | Type |
| :--- | :---------- | :------: | :--: |
| filter |  | ✅ | EntriesFilter! |
            
## Example
```graphql
{
  entries(filter: {keys : "randomString"}) {
    key
    value
    details
  }
}

```