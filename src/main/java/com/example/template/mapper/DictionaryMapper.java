package com.example.template.mapper;

import com.example.template.config.mapper.MappersConfig;
import com.example.template.entity.EntryEntity;
import com.example.template.graphql.types.Entry;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(config = MappersConfig.class, uses = DetailMapper.class)
public interface DictionaryMapper {

    List<Entry> toResponseList(List<EntryEntity> entries);
}
