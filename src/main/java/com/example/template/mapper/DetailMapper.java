package com.example.template.mapper;

import com.example.template.config.mapper.MappersConfig;
import com.example.template.entity.DetailEntity;
import com.example.template.graphql.types.Detail;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(config = MappersConfig.class)
public interface DetailMapper {

    List<Detail> toResponseList(List<DetailEntity> details);
}
