package com.example.template.config.swagger;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

/**
 * Swagger configuration.
 *
 * @author Danila Abdullin
 */
@OpenAPIDefinition(
        info = @Info(
                contact = @Contact(email = "template@mail.com"),
                description = "Dictionary service")
)
public interface SwaggerConfig {
}
