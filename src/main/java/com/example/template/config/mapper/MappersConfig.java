package com.example.template.config.mapper;

import org.mapstruct.MapperConfig;
import org.mapstruct.MappingConstants;

/**
 * Mapper configuration.
 *
 * @author Danila Abdullin
 */
@MapperConfig(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MappersConfig {
}
