package com.example.template.repository;

import com.example.template.entity.EntryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface DictionaryRepository extends JpaRepository<EntryEntity, String> {

    @Query(value = "select entry from EntryEntity entry left join fetch entry.details where entry.key in :keys")
    List<EntryEntity> findAllByIds(@RequestParam("keys") List<String> keys);
}
