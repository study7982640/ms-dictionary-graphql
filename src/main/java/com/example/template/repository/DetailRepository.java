package com.example.template.repository;

import com.example.template.entity.DetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DetailRepository extends JpaRepository<DetailEntity, UUID> {
}
