package com.example.template.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/**
 * Entry entity.
 *
 * @author Danila Abdullin
 */
@Data
@Entity
@Table(name = "details")
public class DetailEntity {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    private UUID id;

    @Column(name = "content", nullable = false, length = 2000)
    private String content;

    @ManyToOne
    @JoinColumn(name = "entry_id", nullable = false)
    private EntryEntity entry;
}
