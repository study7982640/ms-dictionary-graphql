package com.example.template.entity;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Entry entity.
 *
 * @author Danila Abdullin
 */
@Entity
@Data
@Table(name = "entries")
public class EntryEntity {

    @Id
    @Column(name = "key", nullable = false, unique = true)
    private String key;

    @Column(name = "value", nullable = false)
    private String value;

    @OneToMany(mappedBy = "entry", cascade = CascadeType.ALL)
    private List<DetailEntity> details;
}
