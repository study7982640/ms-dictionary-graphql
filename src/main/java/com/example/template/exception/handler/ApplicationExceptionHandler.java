package com.example.template.exception.handler;


import com.example.template.exception.handler.dto.exception.ServerExceptionResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@Log4j2
@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ServerExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        log.error("Not valid arguments", exception);
        String message = exception.getBindingResult().getFieldErrors()
                .stream()
                .findFirst()
                .map((fieldError -> fieldError.getField() + ": " + fieldError.getDefaultMessage()))
                .orElse("Bad request");
        return ServerExceptionResponse.builder()
                .timestamp(LocalDateTime.now())
                .message(message)
                .build();
    }
}
