package com.example.template.exception.handler.dto.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * DTO with information about error.
 *
 * @author Danila Abdullin
 */
@Data
@Builder
public class ServerExceptionResponse {

    @JsonProperty
    @Schema(description = "Timestamp")
    private LocalDateTime timestamp;

    @JsonProperty
    @Schema(description = "Error message")
    private String message;
}
