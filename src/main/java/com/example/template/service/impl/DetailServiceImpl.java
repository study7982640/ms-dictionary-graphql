package com.example.template.service.impl;


import com.example.template.entity.DetailEntity;
import com.example.template.graphql.types.Detail;
import com.example.template.mapper.DetailMapper;
import com.example.template.repository.DetailRepository;
import com.example.template.service.api.DetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DetailServiceImpl implements DetailService {

    private final DetailMapper detailMapper;
    private final DetailRepository detailRepository;

    @Override
    public List<Detail> loadDetails(List<UUID> ids) {
        List<DetailEntity> details = detailRepository.findAllById(ids);
        return detailMapper.toResponseList(details);
    }
}
