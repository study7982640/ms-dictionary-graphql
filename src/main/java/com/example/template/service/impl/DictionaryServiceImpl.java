package com.example.template.service.impl;


import com.example.template.entity.EntryEntity;
import com.example.template.graphql.types.EntriesFilter;
import com.example.template.graphql.types.Entry;
import com.example.template.mapper.DictionaryMapper;
import com.example.template.repository.DictionaryRepository;
import com.example.template.service.api.DictionaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DictionaryServiceImpl implements DictionaryService {

    private final DictionaryMapper dictionaryMapper;
    private final DictionaryRepository dictionaryRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Entry> findEntriesByFilter(EntriesFilter filter) {
        List<EntryEntity> entries = dictionaryRepository.findAllByIds(filter.getKeys());
        return dictionaryMapper.toResponseList(entries);
    }
}
