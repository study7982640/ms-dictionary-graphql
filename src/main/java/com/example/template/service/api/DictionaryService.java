package com.example.template.service.api;

import com.example.template.graphql.types.EntriesFilter;
import com.example.template.graphql.types.Entry;

import java.util.List;

/**
 * Dictionary service.
 *
 * @author Danila Abdullin
 */
public interface DictionaryService {

    /**
     * Get entries by filter.
     *
     * @param filter Filter for find entries.
     * @return List of DTO with information about entry.
     */
    List<Entry> findEntriesByFilter(EntriesFilter filter);
}
