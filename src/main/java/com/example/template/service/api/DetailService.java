package com.example.template.service.api;

import com.example.template.graphql.types.Detail;

import java.util.List;
import java.util.UUID;

/**
 * Detail service.
 *
 * @author Danila Abdullin
 */
public interface DetailService {

    /**
     * Load details by ids.
     *
     * @param ids Details ids.
     * @return List of dto with information about detail.
     */
    List<Detail> loadDetails(List<UUID> ids);
}
