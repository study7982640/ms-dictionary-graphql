package com.example.template.datafetcher;

import com.example.template.graphql.types.EntriesFilter;
import com.example.template.graphql.types.Entry;
import com.example.template.service.api.DictionaryService;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import lombok.RequiredArgsConstructor;

import java.util.List;

@DgsComponent
@RequiredArgsConstructor
public class DictionaryDataFetcher {

    private final DictionaryService dictionaryService;

    @DgsQuery
    public List<Entry> entries(@InputArgument EntriesFilter filter) {
        return dictionaryService.findEntriesByFilter(filter);
    }
}
