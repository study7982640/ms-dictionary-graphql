package com.example.template.datafetcher;

import com.example.template.base.IntegrationTest;
import com.example.template.graphql.types.Entry;
import com.netflix.graphql.dgs.DgsQueryExecutor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DictionaryDataFetcherTest extends IntegrationTest {

    @Autowired
    private DgsQueryExecutor dgsQueryExecutor;

    @Test
    void findEntriesByFilterTest() {
        String key = "DEFAULT";

        Entry[] entries = dgsQueryExecutor.executeAndExtractJsonPathAsObject(
                //language=Graphql
                """
                        {
                            entries(filter: {keys: ["%s"]}) {
                                key
                                value
                                details {
                                    content
                                    id
                                }
                            }
                        }
                        """.formatted(key),
                "data.entries",
                Entry[].class
        );
        assertEquals(1, entries.length);
        assertEquals(entries[0].getKey(), key);
    }
}
